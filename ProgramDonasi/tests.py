from django.http import HttpRequest
from django.test import TestCase
from django.test.client import RequestFactory
from django.urls import resolve
from selenium import webdriver

from .models import UpdateStatus
from .views import *
from .forms import *
import time