from django.conf.urls import url, include
from django.urls import path
from django.contrib.auth.views import LogoutView
import oauth2_provider.views as oauth2_views
from django.conf import settings
from .views import *

urlpatterns = [
	path('login/', login_view, name="login_user"),
	path('signup/', register_view, name='register_user'),
    #re_path(r'^$', index, name='index'),
    # OAuth 2 endpoints:
    #path('', include('social_django.urls', namespace='social')),
    #url(r'^login/$', include('social_django.urls', namespace='social')),
    # url(r'^logout/$', LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
]
