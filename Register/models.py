from django.db import models

class UserAccount(models.Model):
 	username = models.CharField(max_length=64)
 	email = models.EmailField()
 	password = models.CharField(max_length=64)