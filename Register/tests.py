from django.http import HttpRequest
from django.test import TestCase
from django.test.client import RequestFactory
from django.urls import resolve
from selenium import webdriver

from .models import Account
from .views import *
from .forms import *
import time

# class AccountModelTest(TestCase):

#     def create_temp_account(self, 
#     						name="testnm", 
#     						password="testpw", 
#     						email="test@testcase.te.st",
#     						birthDate="2018-12-22"):
#         return Account.objects.create(	name=name, 
#     									password=password, 
#     									email=email,
#     									birthDate=birthDate)

#     def account_creation_test(self):
#         crt = self.create_account()
#         self.assertTrue(isinstance(crt, Account))
#         self.assertEqual(crt.__unicode__(), crt.name)
#         self.assertEqual(crt.__unicode__(), crt.password)
# 		count = Account.objects.all().count()
#         self.assertEqual(count, 1)

# class RegistrationFormTest(TestCase):

# 	def test_valid_form(self):
# 	    c = Account.objects.create(	name="testnm", 
#     								password="Test12341__", 
#     								email="test@testcase.te.st",
#     								birthDate="2018-12-22")
# 	    data = {'name': c.name,
# 	    		'password': c.password,
# 	    		'email':c.email,
# 	    		'birthDate':c.birthDate,}
# 	    form = RegistrationForm(data=data)
# 	    self.assertTrue(form.is_valid())

# 	def test_invalid_form_left_empty(self):
# 	    c = Account.objects.create(name="", 
#     								password="", 
#     								email="",
#     								birthDate="")
# 	    data = {'name': c.name,
# 	    		'password': c.password,
# 	    		'email':c.email,
# 	    		'birthDate':c.birthDate,}
# 	    form = RegistrationForm(data=data)
# 	    self.assertFalse(form.is_valid())

# 	def test_invalid_form_wrong_email(self):
# 	    c = Account.objects.create(name="testtest", 
#     								password="Test12341__", 
#     								email="asdf",
#     								birthDate="2012-12-12")
# 	    data = {'name': c.name,
# 	    		'password': c.password,
# 	    		'email':c.email,
# 	    		'birthDate':c.birthDate,}
# 	    form = RegistrationForm(data=data)
# 	    self.assertFalse(form.is_valid())

# 	def test_invalid_form_name_is_too_short(self):
# 	    c = Account.objects.create(name="te", 
#     								password="Test12341__", 
#     								email="asdf@asdf.com",
#     								birthDate="2012-12-12")
# 	    data = {'name': c.name,
# 	    		'password': c.password,
# 	    		'email':c.email,
# 	    		'birthDate':c.birthDate,}
# 	    form = RegistrationForm(data=data)
# 	    self.assertFalse(form.is_valid())

# 	def test_invalid_form_wrong_pw_format(self):
# 	    c = Account.objects.create(name="te", 
#     								password="test", 
#     								email="asdf@asdf.com",
#     								birthDate="2012-12-12")
# 	    data = {'name': c.name,
# 	    		'password': c.password,
# 	    		'email':c.email,
# 	    		'birthDate':c.birthDate,}
# 	    form = RegistrationForm(data=data)
# 	    self.assertFalse(form.is_valid())

# class AccountUnitTest(TestCase):
# 	def setUp(self):
# 		self.factory = RequestFactory()


# 	def test_registration_page_availability(self):
# 		req = self.factory.get('/auth/login/')
# 		time.sleep(6)
# 		response = load_landing_page(request)
# 		self.assertEqual(response.status_code, 200)

# class AccountViewsTest(TestCase):


# 	def log_in_views_test(self):
# 		c = Account.objects.create(name="testtest", 
#     								password="Test12341__", 
#     								email="asdf",
#     								birthDate="2012-12-12")
# 	    data = {'password': c.password,
# 	    		'email':c.email,
# 	    		}

# 	    request = self.factory.get('/auth/login/', data)
# 	    response = log_in(request)
# 	    self.assertEqual(response.status_code, 200)