from django import forms
from django.contrib.auth import (
	authenticate,
	get_user_model, 
	login, 
	logout,
	)

User = get_user_model()
fieldStyling = (
	'margin-left: auto;'
	+'margin-right: auto;'
	+'margin-top: 10px;'
	+'margin-bottom: 10px;'
	+'padding-left: 25px;'
	+'padding-right: 25px;'
	+'padding-top: 5px;'
	+'padding-top: 5px;'
	+'min-width: 260px;'
	+'justify-content: center;')

class UserLoginForm(forms.Form):

	email = forms.CharField(
		label='',
		required=True,
		widget=forms.EmailInput(attrs={
			'type' : 'email',
			'class' : 'form-control',
			'id' : 'loginEmailInp',
			'aria-describedby' : 'emailHelp',
			'placeholder' : 'Email',
			'style' : fieldStyling,
			})
		)
	password = forms.CharField(
		label='',
		required=True,
		widget=forms.PasswordInput(attrs={
			'type':"password", 
			'class' : "form-control", 
			'id':'loginPassInp',
			'aria-describedby' : 'emailHelp',
			'placeholder' : 'Password',
    		'style' : fieldStyling,
			})
		)

	def clean(self, *args, **kwargs):
		email = self.cleaned_data.get("email")
		password = self.cleaned_data.get("password")

		if email and password:
			user = authenticate(email=email, password=password)
			
			if not user:
				raise forms.ValidationError("This user doesn't exist!")
			
			if not password.check_password(password):
				raise forms.ValidationError("Incorrect password")
		return super(UserLoginForm, self).clean(*args, **kwargs)

class UserRegisterForm(forms.Form):
	email = forms.CharField()
	email_confirm =  forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput)
	display_name = forms.CharField()

	def clean(self, *args, **kwargs):
		if email and email_confirm and password and display_name:
			if email != email_confirm : 
				raise forms.ValidationError("Email is not match with the confirmation Email")

			#if User.objects.get(email=email) not None:
        	#	raise forms.ValidationError('This email address is already in use.')
		return super(UserRegisterForm, self).clean(*args, **args)