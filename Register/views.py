from django.contrib.auth import authenticate, login, logout, get_user_model
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import UserLoginForm, UserRegisterForm

response = {}
def login_view(request):
	response['login_form'] = UserLoginForm()
	if (request.method == 'POST'):
		email = request.POST['email']
		password = request.POST['password']
		response['user'] = authenticate(request, email=email, password=password)
		if response['user'] is not None:
			response['user'].save()
			HttpResponseRedirect("/kasus-bencana/")
		else:
			HttpResponse(status=403)
	else : 
		response['login_form'] = UserLoginForm()
	return render(request, "login_page.html", response)
	#return render(request, "login_page.html", response)

def register_view(request):
	context = {}
	form = UserRegisterForm(request.POST or None)
	if form.is_valid():
		new_account = UserRegisterModel(
			email = request.POST["email"],
			password = request.POST["password"],
			display_name = request.POST["display_name"],
			birthDate = request.POST["birthDate"],
		)
		new_account.save()
		return HttpResponseRedirect(nextState)
	else : 
		form = UserRegisterForm(request.POST or None)
	return render(request, "register_page.html", context)

def logout_view(request):
	logout(request)
	for key in request.session.keys():
		del request.session[key]
	return HttpResponseRedirect('/')


