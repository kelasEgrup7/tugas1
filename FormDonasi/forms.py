from django import forms
from .models import IkutDonasi
from .choices import PROGRAM_DONASI

class FormDonasi(forms.Form):
	nama_program = forms.ChoiceField(
		initial='',
		label='',
		choices=PROGRAM_DONASI,
		required=True,
		widget=forms.Select(attrs={'placeholder':'Pilih program donasi'}))
	nama_donatur = forms.CharField(
		label='',
		widget=forms.TextInput(attrs={'placeholder':'Your name'}))
	email_donatur = forms.EmailField(
		label='',
		widget=forms.TextInput(attrs={'placeholder':'Your e-mail'}),
		required=True)
	nominal = forms.DecimalField(
		label='',
		widget=forms.NumberInput(attrs={'placeholder':'Your donation*'}),
		help_text="(<i>*in Rupiah, ex : 50000</i>)", required=True)
	#anonym = forms.BooleanField(
	#	initial=False,
	#	label='',
	#	required=False,
	#	help_text="Donate as an Anonymous")