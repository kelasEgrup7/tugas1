from django.db import models
from .choices import *

# Create your models here.



class IkutDonasi(models.Model):
	
	nama_program = models.CharField(max_length=80, choices=PROGRAM_DONASI)
	nama_donatur = models.CharField(max_length=32)
	email_donatur = models.EmailField(max_length=64)
	nominal = models.DecimalField(max_digits=16, decimal_places=10)

