from django.http import HttpRequest
from django.test import LiveServerTestCase
from django.test.client import RequestFactory
from django.urls import resolve
from selenium import webdriver

from .models import IkutDonasi
from .views import *
from .forms import *
import time

class DonationTest(LiveServerTestCase):

    port = 8081

    def setUp(self):

        self.driver = webdriver.Firefox()
        self.server = Client().get('http://localhost:8081/form-donasi/')

    def test00ServerExists(self):

        self.assertEqual(self.server.response_code, 200)
    
    

# class DonationModelTest(TestCase):

#     def create_temp_donation(self, 
#     						 event="testnm", 
#     						 donatorsName="testpw", 
#     						 email="test@testcase.te.st",
#     						 nominal=202020, 
#     						 isAnonym=True):
#         return Account.objects.create(  event=event, 
# 			    						donatorsName=donatorsName, 
# 			    						email=email,
# 			    						nominal=nominal, 
# 			    						isAnonym=isAnonym)

#     def donate_creation_test(self):
#         crt = self.create_account()
#         self.assertTrue(isinstance(crt, Donation))
#         self.assertEqual(crt.__unicode__(), crt.event)
# 		count = Donation.objects.all().count()
#         self.assertEqual(count, 1)

# class DonationFormTest(TestCase):

# 	def test_valid_form(self):
# 	    c = Donation.objects.create(	self, 
# 			    						event="testnm", 
# 			    						donatorsName="testpw", 
# 			    						email="test@testcase.te.st",
# 			    						nominal=202020, 
# 			    						isAnonym=True)
# 	    data = {'event'= c.event,
# 	    		'donatorName'=c.donatorName, #default(left empty) : User.name
# 	    		'nominal'=c.nominal,
# 	    		'isAnonym'=c.isAnonym,}
# 	    form = DonationForm(data=data)
# 	    self.assertTrue(form.is_valid())

# 	def test_invalid_form_events_name_left_empty(self):
# 	    c = Donation.objects.create(	self, 
# 			    						event="", 
# 			    						donatorsName="testing", 
# 			    						email="testing@tes.ing",
# 			    						nominal=202020, 
# 			    						isAnonym=True)
# 	    data = {'event'= c.event,
# 	    		'donatorName'=c.donatorName,
# 	    		'nominal'=c.nominal,
# 	    		'isAnonym'=c.isAnonym,}
# 	    form = DonationForm(data=data)
# 	    self.assertFalse(form.is_valid())

# 	def test_invalid_form_nominal_left_empty(self):
# 	    c = Donation.objects.create(	self, 
# 			    						event="testnm", 
# 			    						donatorsName="testpw", 
# 			    						email="test@testcase.te.st",
# 			    						nominal=None, 
# 			    						isAnonym=True)
# 	    data = {'event'= c.event,
# 	    		'donatorName'=c.donatorName,
# 	    		'nominal'=c.nominal,
# 	    		'isAnonym'=c.isAnonym,}
# 	    form = DonationForm(data=data)
# 	    self.assertFalse(form.is_valid())

# 	def test_invalid_form_name_is_too_short(self):
# 	    c = Account.objects.create(name="te", 
#     								password="Test12341__", 
#     								email="asdf@asdf.com",
#     								birthDate="2012-12-12")
# 	    data = {'name': c.name,
# 	    		'password': c.password,
# 	    		'email':c.email,
# 	    		'birthDate':c.birthDate,}
# 	    form = RegistrationForm(data=data)
# 	    self.assertFalse(form.is_valid())

# class AccountUnitTest(TestCase):
# 	def setUp(self):
# 		self.factory = RequestFactory()

# 	def test_donation_page_availability(self):
# 		request = self.factory.get('/donate/')
# 		time.sleep(6)
# 		response = donate(request)
# 		self.assertEqual(response.status_code, 301)

# class DonationViewsTest(TestCase):
# 	def create_donation_views_test(self):
# 		c = Account.objects.create(name="te", 
#     								password="Test12341__", 
#     								email="asdf@asdf.com",
#     								birthDate="2012-12-12")
# 	    data = {'name': c.name,
# 	    		'password': c.password,
# 	    		'email':c.email,
# 	    		'birthDate':c.birthDate,}
# 	    request = self.factory.post('/donate/', data)
# 		response = donate(request)
# 	    self.assertEqual(response.status_code, 301) #redirected to news page




#>>> USER AUTHENTICATION TEST : 
#>>> 1. user not logged in but accessed to donation link -> will be directed to login page
#>>> 2. user logged in, then web can access api to retrieve name, email