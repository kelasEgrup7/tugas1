from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import IkutDonasi
from .forms import FormDonasi

response = {}
		
def index(request):
  	if request.user.is_authenticated:
  		print(request.user)
  	response['retrieved_donations'] = IkutDonasi.objects.all()
  	response['title'] = 'Donation Submission'
  	response['get_donation_form'] = FormDonasi(request.POST or None)
  	if (request.method == 'POST'):
  		if (response['get_donation_form'].is_valid()):
  			response['nama_donatur'] = request.POST['nama_donatur']
  			response['nama_program'] = request.POST['nama_program']
  			response['email_donatur'] = request.POST['email_donatur']
  			response['nominal'] = request.POST['nominal']
  			ikut_donasi = IkutDonasi(nama_program=response['nama_program'],
						  			 nama_donatur=response['nama_donatur'],
						   			 email_donatur=response['email_donatur'],
						   			 nominal=response['nominal'])
  			ikut_donasi.save()
  	return render(request, 'FormDonasi.html', response)

# Create your views here.