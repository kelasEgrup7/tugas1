"""tugas1e7 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.auth.views import LogoutView
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import re_path, path
from django.views.generic.base import RedirectView
from Register.views import *



urlpatterns = [
    path('admin/', admin.site.urls), 
    path('', RedirectView.as_view(url='kasus-bencana/')),
    re_path(r'^kasus-bencana/', include(('KasusBencana.urls','ProgramDonasi'), namespace='KasusBencana')),
	re_path(r'^program-donasi/', include(('ProgramDonasi.urls','ProgramDonasi'), namespace='ProgramDonasi')),
	re_path(r'^form-donasi/', include(('FormDonasi.urls', 'FormDonasi'), namespace='FormDonasi')),
    path('account/', include(('Register.urls', 'account'), namespace='account')),
    path('auth/', include('social_django.urls', namespace='social')),
    url(r'^auth/logout/$',logout_view,  name='logout'),
]
